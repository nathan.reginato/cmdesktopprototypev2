import React from "react";
import createReactClass from "create-react-class";
import PhotoArea from "./photoArea/photoArea";
import { message } from 'antd'

let App = createReactClass({
  displayName: "App",

  // getInitialState :: { error :: String }
  getInitialState: function() {return {error: ""}},

  // showError :: String -> State Error
  showError: function(e) { 
    this.setState({error: e.message});
    message.error(e.message);
  },

  render() {
    return (
    <div id="App">
      <PhotoArea showError={this.showError} />
    </div>
  )}
});

export default App;
