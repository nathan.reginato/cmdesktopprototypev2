import React from "react";
import createReactClass from "create-react-class";
import { getURLFromPhoto, getURL } from "./model";
import "./photo.css"; 


const Photo = createReactClass({
    displayName: "Photo",

    // getInitialState :: { photo: {} }
    getInitialState() { 
        return { 
            photo: this.props.photo,
            url: "",
        }
    },

    componentDidMount() {
        getURLFromPhoto(this.props.photo).fork(null, (url) => this.setState({ url: getURL(url) }))
    },

    selectPhoto(_) {
        this.setState({photo: { selected: !this.state.photo.selected }})
        console.log(this.state.photo.selected)
        
        this.props.onSelected(this.props.photo)
    },


    render() {
        return(
            <div 
                onClick={this.selectPhoto} 
                className={this.state.photo.selected ? "photo selected" : "photo"}>
                <img src={this.state.url} alt=""/>
            </div>
        )
    }
});

export default Photo;