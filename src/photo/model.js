import { compose, prop, map } from "ramda";
import Task from "data.task";

const fileReader = {
    // read :: File -> Task Error response
    read: function(file) {
        return new Task((rej, res) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = res;
        })
    }
}

// getURL :: result -> URL 
const getURL = compose(prop('result'), prop('target'));

// getURLFromPhoto :: Photo -> Task Error URL 
const getURLFromPhoto = compose(fileReader.read, prop("file"))


module.exports = { getURLFromPhoto, getURL }