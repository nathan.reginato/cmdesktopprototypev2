import React from "react";
import createReactClass from "create-react-class";
import { Input, Pagination } from 'antd';
import Photo from "../photo/photo"
import { getPhotosFromEvent, getLoadedPhotos, numberOfLoadededPhotos } from "./model";
import { append } from "ramda";
import "./photoArea.css";

const Search = Input.Search;

const StoryBoard = createReactClass({
    displayName: "PhotoArea",

    // getInitialState :: { localPhotos :: [], loadedPhotos :: [] }
    getInitialState() { return { localPhotos: [], loadedPhotos: [], selectedPhotos: [], loadedPhotoIndex: 0 } },

    // fileUpload :: Event -> State localPhotos
    fileUpload(event) {
        this.setState({localPhotos: getPhotosFromEvent(event)}, () => 
        this.setState({
            loadedPhotos: getLoadedPhotos(this.state.localPhotos, this.state.loadedPhotoIndex),
            loadedPhotoIndex: this.state.loadedPhotoIndex + numberOfLoadededPhotos
        }))
    },

    onPhotoSelected(photo) {
        this.setState({selectedPhotos: append(photo, this.state.selectedPhotos) })
    },

    render() {

        
        return(
            <div className={"workspace"}>
                <input
                type="file" 
                multiple
                onChange={this.fileUpload} />
                <div>
                    {this.state.localPhotos.map(photo => 
                        <Photo onSelected={this.onPhotoSelected} photo={photo} />
                    )}
                </div>
            </div>
     
        )
    }
});

export default StoryBoard;