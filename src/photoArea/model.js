import { values, compose, map, prop, slice } from "ramda";
import Task from "data.task";

const Photo = {}

// createPhoto :: file -> Photo
const createPhoto = function(file) {
    return {
        file: file,
        tags: [],
        selected: false,
        viewable: false
    }
}

const numberOfLoadededPhotos = 20

// getFilesArrayFromObject :: {File} -> [File]
const getFilesArrayFromObject = values

// getPhotos :: {File} -> [Photo]
const getPhotos = compose(map(createPhoto), getFilesArrayFromObject)

// getFiles :: event -> {File}
const getFiles = compose(prop("files"), prop("target"))

// getPhotosFromEvent :: event -> [Photo]
const getPhotosFromEvent = compose(getPhotos, getFiles)

// getInitialLoadedPhotos :: [Photo] -> [Photo]
const getLoadedPhotos = (index, photos) => slice(index, numberOfLoadededPhotos, photos)

module.exports = { getPhotosFromEvent, getLoadedPhotos, numberOfLoadededPhotos }